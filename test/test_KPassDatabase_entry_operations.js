// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-03-23T10:10:13
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>


import { expect } from "chai";
import { unlink } from "node:fs/promises";
import { KPassDatabase } from "../src/index.js";

describe("KPassDatabase entry operations", function() {
  /** @type {KPassDatabase} */
  let db;

  beforeEach(async function() {
    await unlink("./db.kdbx").catch(() => undefined);

    await KPassDatabase.create("./db.kdbx");
    db = new KPassDatabase("./db.kdbx");
    await db.open();
  });

  afterEach(async function() {
    db.close();
    // @ts-ignore
    db = null;
  });

  it("can send raw commands", async function() {
    const ret = await db.sendRaw("add test");
    expect(ret).to.equal("Successfully added entry test.");
  });

  it("can add a basic entry", async function() {
    expect(await db.addEntry("test", "my-password"))
    .to.equal("Successfully added entry test.");

    expect(await db.getEntryAttribute("test"))
    .to.equal("my-password");

    expect(await db.deleteEntry("test"))
    .to.equal("Successfully deleted entry test.");
  });

  it("fails to add [empty] entries", async function() {
    await db.addEntry("[empty]", "xxx")
    .then(
      () => expect.fail("[empty] should not be an allowed name"),
      (err) => expect(err?.message).to.contain("kpass error"));

    await db.createGroup("test");
    await db.addEntry("/test/[empty]", "xxx")
    .then(
      () => expect.fail("[empty] should not be an allowed name"),
      (err) => expect(err?.message).to.contain("kpass error"));
  });

  it("can delete entries", async function() {
    await db.addEntry("test", "my-password");

    expect(await db.deleteEntry("test"))
    .to.equal("Successfully deleted entry test.");

    await db.deleteEntry("test")
    .then(
      () => expect.fail("should fail to delete non-existing entry"),
      (err) => expect(err?.message).to.contain("kpass error"));
  });

  it("fails to add existing entry", async function() {
    this.timeout(KPassDatabase.EditTimeout + 250);
    expect(await db.addEntry("test", "my-password"))
    .to.equal("Successfully added entry test.");

    await db.addEntry("test", "my-password")
    .then(
      () => expect.fail("should fail to add existing entry"),
      (err) => expect(err?.message).to.contain("kpass error"));
  });

  it("can add an entry with additional attributes", async function() {
    expect(await db.addEntry("test", "my-password", {
      notes: "this is \nsome \nnotes",
      url: "and an URL",
      username: "sbob"
    }))
    .to.equal("Successfully added entry test.");

    expect(await db.getEntryAttribute("test", "notes"))
    .to.equal("this is \nsome \nnotes");
  });

  it("can edit an entry", async function() {
    this.timeout((KPassDatabase.EditTimeout * 2) + 250);
    await db.addEntry("test", "my-password", { url: "test-url" });

    expect(await db.editEntry("test", null, { url: "another-url" }))
    .to.equal("Successfully edited entry test.");

    expect(await db.getEntryAttribute("test", "url"))
    .to.equal("another-url");

    expect(await db.editEntry("test", "new-pass"))
    .to.equal("Successfully edited entry test.");

    expect(await db.getEntryAttribute("test", "url"))
    .to.equal("another-url");
    expect(await db.getEntryAttribute("test", "password"))
    .to.equal("new-pass");

    await db.editEntry("unknown", null, { url: "another" })
    .then(
      () => expect.fail("should fail to edit unknown entry"),
      (err) => expect(err?.message).to.contain("kpass error"));

    await db.editEntry("unknown", "test")
    .then(
      () => expect.fail("should fail to edit unknown entry"),
      (err) => expect(err?.message).to.contain("kpass error"));
  });

  it("can get a complete entry", async function() {
    await db.addEntry("test", "my password", {
      url: "http://cern.ch",
      username: "sbob",
      notes: "some\nfancy\nnotes😎"
    });

    expect(await db.getEntry("/test"))
    .to.deep.contain({
      Title: "test",
      Tags: "",
      Password: "my password",
      URL: "http://cern.ch",
      UserName: "sbob",
      Notes: "some\nfancy\nnotes😎"
    });
  });
});
