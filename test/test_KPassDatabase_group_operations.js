// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-03-23T10:10:13
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>


import { expect } from "chai";
import { unlink } from "node:fs/promises";
import { KPassDatabase, KPassElementType } from "../src/index.js";

describe("KPassDatabase group operations", function() {
  /** @type {KPassDatabase} */
  let db;

  beforeEach(async function() {
    await unlink("./db.kdbx").catch(() => undefined);

    await KPassDatabase.create("./db.kdbx");
    db = new KPassDatabase("./db.kdbx");
    await db.open();
  });

  afterEach(async function() {
    db.close();
    // @ts-ignore
    db = null;
  });

  it("can create groups", async function() {
    expect(await db.createGroup("test"))
    .to.equal("Successfully added group test.");

    expect(await db.createGroup("test/recurse"))
    .to.equal("Successfully added group recurse.");

    await db.createGroup("test")
    .then(
      () => expect.fail("should fail to create existing group"),
      (err) => expect(err?.message).to.contain("kpass error"));
  });

  it("can create groups recursively", async function() {
    await db.createGroup("test/toto/titi")
    .then(
      () => expect.fail("should fail to create nested group when recursive is not set"),
      (err) => expect(err?.message).to.contain("kpass error"));

    expect(await db.createGroup("test/toto/titi", true))
    .to.equal("Successfully added group titi.");
    expect(await db.getType("test/toto/titi")).to.equal(KPassElementType.GROUP);

    await db.addEntry("/test/toto/titi/entry", "password");
    await db.createGroup("test/toto/titi/entry", true)
    .then(
      () => expect.fail("should fail to group if entry exists with same name"),
      (err) => expect(err?.message).to.contain("kpass error"));

    await db.addEntry("/test/entry", "password");
    await db.createGroup("test/entry/titi/tata", true)
    .then(
      () => expect.fail("should fail to group if entry exists with same name"),
      (err) => expect(err?.message).to.contain("kpass error"));
  });

  it("can create groups (special cases)", async function() {
    for (const recurse of [ true, false ]) {
      for (const path of [ "", "/" ]) {
        await db.createGroup(path, recurse)
        .then(
          () => expect.fail(
            `should fail to create empty/root group "${path}" ${recurse}`),
          (err) => expect(err?.message).to.contain("exist"));
      }
    }
  });

  it("can delete groups", async function() {
    await db.createGroup("test");

    expect(await db.deleteGroup("test"))
    .to.equal("Successfully deleted group test.");

    expect(await db.getType("test")).to.equal(null);
  });

  it("can delete groups recursively", async function() {
    await db.createGroup("test/titi/toto", true);
    await db.addEntry("test/titi/toto/entry", "pass");

    /* delete is always recursive, no additional args */
    expect(await db.deleteGroup("test"))
    .to.equal("Successfully deleted group test.");

    expect(await db.getType("test")).to.equal(null);
  });
});
