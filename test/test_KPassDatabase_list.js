// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-03-23T10:10:13
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>


import { expect } from "chai";
import { unlink } from "node:fs/promises";
import { KPassDatabase } from "../src/index.js";
import { parse as pathParse } from "node:path";

describe("KPassDatabase list groups", function() {
  /** @type {KPassDatabase} */
  let db;

  beforeEach(async function() {
    await unlink("./db.kdbx").catch(() => undefined);

    await KPassDatabase.create("./db.kdbx");
    db = new KPassDatabase("./db.kdbx");
    await db.open();

    for (const entry of [
      { path: "test", password: "xxx", attributes: { url: "yyy" } },
      { path: "toto/test", password: "xxx", attributes: { url: "yyy" } },
      { path: "toto/test2", password: "xxx", attributes: { url: "xxx" } },
      { path: "toto/titi/" }
    ]) {
      const dir = pathParse(entry.path).dir;
      if (await db.getType(dir) === null) {
        await db.createGroup(dir, true);
      }
      if (entry.path.endsWith("/")) {
        await db.createGroup(entry.path, true);
      }
      else {
        await db.addEntry(entry.path, entry.password ?? "", entry.attributes);
      }
    }
  });

  afterEach(async function() {
    db.close();
    // @ts-ignore
    db = null;
  });

  it("can list db content", async function() {
    expect(await db.listGroup("/toto"))
    .to.deep.equal({
      groups: [ "/toto/titi" ],
      entries: [ "/toto/test", "/toto/test2" ]
    });

    expect(await db.listGroup("/toto/titi"))
    .to.deep.equal({ groups: [], entries: [] });
  });

  it("can list db content recursively", async function() {
    expect(await db.listGroup("/", true))
    .to.deep.equal({
      groups: [ "/toto", "/toto/titi" ],
      entries: [ "/test", "/toto/test", "/toto/test2" ]
    });
  });
});
