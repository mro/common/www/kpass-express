// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-04-01T18:15:32
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>

import { expect } from "chai";
import { percent } from "../src/utils.js";

describe("utils", function() {

  it("can percent encode/decode", async function() {
    [
      { decoded: "this\nis some\t% encoded text",
        encoded: "this%0Ais%20some%09%25%20encoded%20text" },
      { decoded: "🎉", encoded: "%F0%9F%8E%89" }
    ].forEach((test) => {
      expect(percent.encode(test.decoded)).to.equal(test.encoded);
      expect(percent.decode(test.encoded)).to.equal(test.decoded);
    });
  });
});
