// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-03-23T10:10:13
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>


import { expect } from "chai";
import { unlink } from "node:fs/promises";
import { KPassDatabase } from "../src/index.js";

describe("KPassDatabase creation", function() {

  it("can create a database without password", async function() {
    await unlink("./db.kdbx").catch(() => undefined);

    await KPassDatabase.create("./db.kdbx");

    await KPassDatabase.create("./db.kdbx")
    .then(
      () => expect.fail("should fail to call create on existing db"),
      (err) => expect(err).to.have.property("message").that.contains("exists")
    );
  });

  it("can create a database with password", async function() {
    await unlink("./db.kdbx").catch(() => undefined);

    await KPassDatabase.create("./db.kdbx", "my-secret");
  });
});

describe("KPassDatabase client", function() {
  /** @type {KPassDatabase|null} */
  let db;

  beforeEach(async function() {
    await unlink("./db.kdbx").catch(() => undefined);

    await KPassDatabase.create("./db.kdbx");
  });


  afterEach(async function() {
    db?.close();
    db = null;
  });


  it("can connect a database", async function() {
    db = new KPassDatabase("./db.kdbx");

    await db.open();
    await db.close();
  });

  it("fails to connect with invalid password", async function() {
    db = new KPassDatabase("./db.kdbx", { password: "invalid" });

    await db.open()
    .then(
      () => expect.fail("should fail to open database"),
      (err) => expect(err).to.have.property("message").that.contains("invalid")
    );
  });
});
