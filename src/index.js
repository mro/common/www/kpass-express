/* eslint-disable max-lines */
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-03-23T09:29:43
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>

import { execFile, spawn } from "node:child_process";
import { parse as pathParse } from "node:path";
import { LineBuffer, VarBuffer, isNil, prom } from "@cern/nodash";
import { percent } from "./utils.js";
import d from "debug";

const debug = d("kpass");
const debugMsg = process.env.NODE_ENV === "production" ?
  () => null : d("kpass:msg");

/**
 * @template T
 * @typedef {import("@cern/nodash").Deferred<T>} Deferred<T>
 */

/**
 * @typedef {import("node:child_process").ChildProcess} ChildProcess
 * @typedef {Partial<{
 *    password: string
 * }>} KPassDatabaseOptions
 * @typedef {"password" | "title" | "username" | "notes" | string} KPassAttribute
 * @typedef {{ entries: string[], groups: string[] }} KPassGroupInfo
 */

/**
 * @param {string} value
 * @returns {string}
 */
function escape(value) {
  return JSON.stringify(value);
}

/**
 * @param {string} expected
 * @return {(value: string) => string}
 */
function checkResult(expected) {
  return function(value) {
    if (!value?.startsWith?.(expected) ?? false) {
      const msg = `kpass error: ${value || "command failed"}`;
      debug(msg);
      throw new Error(msg);
    }
    return value;
  };
}

/**
 * @param {string} path
 */
function normalize(path) {
  if (path === "/") {
    return path;
  }
  else if (path.endsWith("/")) {
    path = path.substring(0, path.length - 1);
  }
  if (!path.startsWith("/")) {
    path = "/" + path;
  }
  return path;
}

export const DefaultAttributes = new Set([
  "Title", "UserName", "Password", "URL", "Notes"
]);

/**
 * @enum {number}
 */
export const KPassElementType = {
  GROUP: 1,
  ENTRY: 2
};

class KPassCommandContext {
  /** @type {Deferred<Uint8Array|string>} */
  deferred = prom.makeDeferred();

  /** @type {boolean} */
  rawBuffer = false;

  /** @type {number} */
  lastCommandSize;

  /**
   * @param {string} cmd
   */
  constructor(cmd, rawBuffer = false) {
    this.rawBuffer = rawBuffer;
    this.lastCommandSize = cmd.length + 1;
  }

  get promise() {
    return this.deferred.promise;
  }

  get isPending() {
    return this.deferred.isPending;
  }

  /**
   * @param {string|Uint8Array} value
   */
  resolve(value) {
    this.deferred.resolve(value);
  }

  /**
   * @param {any} error
   */
  reject(error) {
    this.deferred.reject(error);
  }
}

export class KPassDatabase {
  /** @type {string} */
  #path;

  /** @type {KPassDatabaseOptions|null} */
  #options = null;

  /** @type {ChildProcess|null} */
  #spawn = null;

  /** @type {ReturnType<prom.makeDeferred>|null} */
  #spawnDef = null;

  #buffer = new VarBuffer();

  /** @type {KPassCommandContext|null} */
  #context = null;

  #prompt = "";

  #hasEncoding = false;

  static EditTimeout = 2000;

  static KPassXCCli = process.env["KPASS_CLI"] ?? "keepassxc-cli";

  /**
   * @param {string} path
   * @param {KPassDatabaseOptions|null} options
   */
  constructor(path, options = null) {
    this.#path = path;
    this.#prompt = pathParse(path).base + "> ";

    this.#options = options;
  }

  /**
   * @brief create a new database file
   * @param {string} path
   * @param {string} [password]
   */
  static async create(path, password = "") {
    const def = prom.makeDeferred();
    const sp = execFile(KPassDatabase.KPassXCCli, [ "db-create", "-q", "-p", path ], {},
      (err, stdout, stderr) => {
        if (err) {
          debug("error creating kpass db: ", stdout, stderr);
          def.reject(err);
        }
        else {
          debug("kpass db created");
          def.resolve(undefined);
        }
      });
    sp.stdout?.on("data", (data) => debug("msg.stdout: ", data?.toString()));
    sp.stderr?.on("data", (data) => debug("msg.stderr: ", data?.toString()));
    if (password) {
      sp.stdin?.write(password + "\n");
      sp.stdin?.write(password + "\n");
    }
    else {
      sp.stdin?.write("\ny\n");
    }
    return def.promise;
  }

  async open() {
    if (this.#spawn) {
      await this.close();
    }

    const def = prom.makeDeferred();
    const sp = spawn(KPassDatabase.KPassXCCli, [ "open", this.#path ], { detached: true });
    function cleanup() {
      sp.removeListener("close", onClose);
      sp.stdout.removeListener("data", onStdout);
      sp.stderr.removeListener("data", onStderr);
    }
    const onClose = (/** @type {number|null} */ err) => {
      debug("failed to start:", err);
      cleanup();
      def.reject(err);
    };
    const onStdout = (/** @type {Buffer} */ data) => {
      const line = data.toString();
      debugMsg("msg[1][open]: %s", line);

      if (line.startsWith(this.#prompt)) {
        cleanup();
        if (this.#spawn) {
          sp.kill("SIGTERM");
          def.reject(new Error("already open"));
          return;
        }

        debug("keepass database open: %s", this.#path);
        this.#spawn = sp;
        this.#spawnDef = prom.makeDeferred();
        sp.once("error", (err) => this.#onError(err));
        sp.once("close", (code, signal) => this.#onClose(code, signal));
        sp.stdout.on("data", (data) => this.#onData(data));
        sp.stderr.on("data", (data) => this.#onStdErr(data));
        def.resolve(undefined);
      }
    };
    const onStderr = (/** @type {Buffer} */ data) => {
      const line = data.toString().trim();
      debugMsg("msg[2][open]: %s", line);

      if (line.startsWith("Enter password")) {
        sp.stdin?.write(this.#options?.password ?? "");
        sp.stdin?.write("\n");
      }
      else if (line.includes("Invalid credentials")) {
        cleanup();
        sp.kill("SIGTERM");
        def.reject(new Error("invalid credentials"));
      }
    };

    sp.on("close", onClose);
    sp.stdout.on("data", onStdout);
    sp.stderr.on("data", onStderr);
    return def.promise
    .then(() => this.testCliFeatures());
  }

  /**
   *
   * @param {number} killDelay TERM timeout before killing
   */
  async close(killDelay = 5000) {
    const def = this.#spawnDef;
    debug("sending: SIGTERM");
    this.#spawn?.kill("SIGTERM");

    if (def?.promise) {
      return prom.timeout(def.promise, killDelay)
      .catch((err) => {
        if (err?.code === "ETIMEDOUT") {
          debug("sending: SIGKILL");
          this.#spawn?.kill("SIGKILL");
          return this.#spawnDef?.promise;
        }
        throw err;
      });
    }
    else {
      return Promise.resolve();
    }
  }

  async testCliFeatures() {
    const ret = await this.sendRaw("help show");
    this.#hasEncoding = ret.includes("--encoding");

    debug(`encoding argument is ${this.#hasEncoding ? "" : "not "}supported by ${KPassDatabase.KPassXCCli}`);
  }

  /**
   * @param {Buffer} data
   */
  #onData(data) {
    this.#buffer.add(data);
    if (data.indexOf(this.#prompt, -this.#prompt.length) >= 0) {

      if (this.#context) {

        const payload = this.#buffer.subarray(
          this.#context.lastCommandSize,
          this.#buffer.length - this.#prompt.length);
        this.#buffer.reset();

        const cmd = this.#context;
        this.#context = null;
        if (cmd.rawBuffer) {
          debugMsg("msg[1][result]: payload.byteLength: %i", payload.length);
          cmd.resolve(payload);
        }
        else {
          const ret = this.#buffer.decode(payload).trim();
          debugMsg("msg[1][result]: payload: %s", ret);
          cmd.resolve(ret);
        }
      }
      else {
        debugMsg("msg[1][drop]: buffer.length:%i", this.#buffer.length);
        this.#buffer.reset();
      }
    }
  }

  /**
   * @param {Buffer} data
   */
  #onStdErr(data) {
    debugMsg("msg[2]: %s", data?.toString());
  }

  /**
   * @param {Error} err
   */
  #onError(err) {
    debug("error: %s", err?.message ?? err);
  }

  /**
   * @param {number|null} code
   * @param {string|null} signal
   */
  #onClose(code, signal) {
    debug("closed: %s", Number.isFinite(code) ? code : signal);
    this.#spawn?.removeAllListeners();
    this.#spawn?.stdin?.removeAllListeners();
    this.#spawn?.stdout?.removeAllListeners();

    this.#spawn = null;
    const spawnDef = this.#spawnDef;
    this.#spawnDef = null;
    if (!code) {
      spawnDef?.resolve(code);
    }
    else {
      spawnDef?.reject(new Error(`keepassxc-cli exited with: ${code}`));
    }
  }

  /**
   * @template {boolean} [T=false]
   * @param {string} cmd command to send
   * @param {T} rawBuffer retrieve raw buffer
   * @param {number} timeout command timeout
   * @returns {Promise<T extends true ? Uint8Array : string>}
   */
  async sendRaw(cmd, rawBuffer = /** @type {T} */ (false), timeout = KPassDatabase.EditTimeout) {
    if (!this.#spawn) {
      throw new Error("kpass error: database not open");
    }
    else if (this.#context?.isPending) {
      throw new Error("kpass error: command already in progress");
    }
    this.#context = new KPassCommandContext(cmd, rawBuffer);

    this.#spawn.stdin?.write(cmd);
    this.#spawn.stdin?.write("\n");
    debug("cmd: %s", cmd);
    debugMsg("msg[0]: cmd: %s", cmd);

    return /** @type {Promise<T extends true ? Uint8Array : string>} */ (
      prom.timeout(
        this.#context.deferred ?? Promise.reject("internal error"),
        timeout,
        "kpass error: command timeout"));
  }


  /**
   * @param {string} path
   * @param {string} password
   * @param {Partial<{
   *   notes: string,
   *   url: string,
   *   username: string
   *  }>} [attrs]
   */
  async addEntry(path, password, attrs = undefined) {
    /* FIXME should we escape password if it contains \n ? */
    path = normalize(path);
    if (path.endsWith("/[empty]")) {
      throw new Error(`kpass error: ${path} is not an authorized name`);
    }

    let cmd = (this.#hasEncoding) ? "add -p --encoding percent" : "add -p";
    if (attrs) {
      Object.entries(attrs).forEach(([ key, value ]) => {
        if (this.#hasEncoding) {
          value = percent.encode(value);
        }
        else if (key === "notes") {
          /* see https://github.com/keepassxreboot/keepassxc/blob/514afebcc79cebebbcec38f3670a4b0ce0001de3/src/cli/Add.cpp#L110 */
          value = value.replaceAll("\n", "\\n");
        }
        cmd += ` --${key} ${escape(value)}`;
      });
    }
    const def = prom.makeDeferred();
    this.#spawn?.stdout?.on("data", (data) => {
      if (data.toString().endsWith("Enter password for new entry: ")) {
        this.#buffer.reset();
        if (this.#context) {
          this.#context.lastCommandSize = 0;
        }
        def.resolve(undefined);
      }
    });
    const ret = this.sendRaw(`${cmd} ${escape(path)}`);
    await prom.timeout(def, KPassDatabase.EditTimeout,
      "kpass error: password-prompt timeout");
    await this.#spawn?.stdin?.write(`${password}\n`);

    return ret
    .then(checkResult("Successfully added entry"));
  }

  /**
   * @param {string} path
   * @param {KPassAttribute} attribute
   */
  async getEntryAttribute(path, attribute = "password") {
    const cmd = `show -s -a ${escape(attribute)}`;

    if (this.#hasEncoding) {
      return this.sendRaw(`${cmd} --encoding percent ${escape(path)}`)
      .then((ret) => percent.decode(ret));
    }
    else {
      return this.sendRaw(`${cmd} ${escape(path)}`);
    }
  }

  /**
   * @param {string} path
   */
  async getEntry(path) {
    let cmd = "show -s --all";

    if (this.#hasEncoding) {
      cmd += " --encoding percent";
    }

    const ret = await this.sendRaw(`${cmd} ${escape(path)}`, true);
    const parser = new LineBuffer(ret);

    /** @type {string} */
    let last = "";
    return [ ...parser.lines() ].reduce((ret, line) => {
      const idx = line.indexOf(":");
      if (idx >= 0) {
        last = line.substring(0, idx);
        const value = line.substring(idx + 2, line.length - 1);
        ret[last] = this.#hasEncoding ? percent.decode(value) : value;
      }
      else if (ret[last] !== undefined) {
        ret[last] += "\n";
        const value = line.substring(0, line.length - 1);
        ret[last] += this.#hasEncoding ? percent.decode(value) : value;
      }
      return ret;
    }, /** @type {{ [key: string]: string }} */ ({}));
  }

  /**
   * @param {string} path
   */
  async deleteEntry(path) {
    return this.sendRaw(`rm ${escape(path)}`)
    .then((ret) => {
      if (ret.includes("recycled")) {
        /* make delete permanent */
        return this.sendRaw(`rm ${escape(path)}`);
      }
      return ret;
    })
    .then(checkResult("Successfully deleted entry"));
  }

  /**
   *
   * @param {string} path
   * @param {string|null} password
   * @param {Partial<{
   *   title: string,
   *   notes: string,
   *   url: string,
   *   username: string
   *  }>} [options]
   */
  async editEntry(path, password, options = undefined) {
    let cmd = isNil(password) ? "edit" : "edit -p";

    if (this.#hasEncoding) {
      cmd += " --encoding percent";
    }
    if (options) {
      Object.entries(options).forEach(([ key, value ]) => {
        if (this.#hasEncoding) {
          value = percent.encode(value);
        }
        else if (key === "notes") {
          value = value.replaceAll("\n", "\\n");
        }
        cmd += ` --${key} ${escape(value)}`;
      });
    }

    if (!isNil(password)) {
      const def = prom.makeDeferred();
      this.#spawn?.stdout?.on("data", (data) => {
        if (data.toString().endsWith("Enter new password for entry: ")) {
          this.#buffer.reset();
          if (this.#context) {
            this.#context.lastCommandSize = 0;
          }
          def.resolve(undefined);
        }
      });
      const ret = this.sendRaw(`${cmd} ${escape(path)}`);
      await prom.timeout(def, KPassDatabase.EditTimeout,
        "kpass error: password-prompt timeout");
      this.#spawn?.stdin?.write(`${password}\n`);

      return ret
      .then(checkResult("Successfully edited entry"));
    }
    else {
      return this.sendRaw(`${cmd} ${escape(path)}`)
      .then(checkResult("Successfully edited entry"));
    }
  }

  /**
   * @param {string} path
   * @param {boolean} recursive
   */
  // eslint-disable-next-line complexity
  async createGroup(path, recursive = false) {
    path = normalize(path);
    if (await this.getType(path) !== null) {
      const msg = `kpass error: Failed to create group, ${path || "/"} exists`;
      debug(msg);
      throw new Error(msg);
    }
    else if (recursive) {
      let p = "";
      const dirs = [];
      for (const part of path.split("/")) {
        if (!part) { continue; }
        p += "/" + part;
        const type = await this.getType(p);
        if (type === KPassElementType.ENTRY) {
          const msg = `kpass error: Failed to create group, ${p} is an entry`;
          debug(msg);
          throw new Error(msg);
        }
        else if (type === null) {
          dirs.push(p);
        }
      }

      let ret;
      for (const d of dirs) {
        ret = await this.sendRaw(`mkdir ${escape(d)}`)
        .then(checkResult("Successfully added group"));
      }
      /* special case when calling `createGroup("/", true)` */
      return ret || "Successfully added group";
    }
    else {
      return this.sendRaw(`mkdir ${escape(path)}`)
      .then(checkResult("Successfully added group"));
    }
  }

  /**
   * @param {string} path
   */
  async deleteGroup(path) {
    path = normalize(path);
    return this.sendRaw(`rmdir ${escape(path)}`)
    .then(checkResult("Successfully recycled group"))
    .then(() => {
      const recyclePath = `/Recycle Bin/${pathParse(path).base}`;
      return this.sendRaw(`rmdir ${escape(recyclePath)}`)
      .then(checkResult("Successfully deleted group"))
      .then((ret) => ret.replace("/Recycle Bin/", ""));
    });
  }

  /**
   * @param {string} path
   * @param {boolean} recursive
   * @return {Promise<KPassGroupInfo>}
   */
  async listGroup(path = "/", recursive = false) {
    path = normalize(path);
    path = path.endsWith("/") ? path : (path + "/");

    /** @type {KPassGroupInfo} */
    const ret = { entries: [], groups: [] };
    const parser = new LineBuffer(await this.sendRaw(
      `ls -f${recursive ? " -R" : ""} ${escape(path)}`, true));

    for (let line of parser.lines()) {
      line = line.trim();
      if (line.endsWith("/")) {
        ret.groups.push(path + line.substring(0, line.length - 1));
      }
      else if (!line.endsWith("[empty]")) {
        ret.entries.push(path + line);
      }
    }
    return ret;
  }

  /**
   * @brief get entry type
   * @param {string} path
   * @return {Promise<KPassElementType|null>}
   */
  async getType(path) {
    const dirOnly = path.endsWith("/");

    path = normalize(path);
    if (path === "/" || !path) {
      return KPassElementType.GROUP;
    }

    const parent = pathParse(path).dir;

    const content = await this.listGroup(parent);
    if (!dirOnly && content.entries.includes(path)) {
      return KPassElementType.ENTRY;
    }
    else if (content.groups.includes(path)) {
      return KPassElementType.GROUP;
    }
    return null;
  }
}

