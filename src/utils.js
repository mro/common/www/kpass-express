// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 Sylvain Fargier
// SPDX-Created: 2024-04-01T17:37:13
// SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>

export const percent = {
  /**
   * @param {string} value
   * @details see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent#encoding_for_rfc3986
   */
  encode(value) {
    return encodeURIComponent(value).replace(/[!'()*]/g,
      (c) => `%${c.charCodeAt(0).toString(16).toUpperCase()}`);
  },
  /** @param {string} value */
  decode(value) {
    return decodeURIComponent(value);
  }
};
