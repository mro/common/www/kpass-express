# Express middleware for KeePass-XC

[Express](https://expressjs.com/) compatible middleware for [KeePass-XC](https://keepassxc.org/) databases.

## Known Issues

- entry must not be named `[empty]`
- default attributes are case-insensitive when custom aren't

## TODO list

- [ ] search
- [ ] generate
- [ ] get complete entry
    - modify keepassxc-cli to base64 encode values ?
- [ ] list attachments
    - modify keepassxc-cli to list attachments
- [ ] export attachments
- [ ] get db info